# Review

## Info
Reviewer Name:
Qiyang Huang
Paper Title: 
ReVirt: Enabling Intrusion Analysis through Virtual-Machine Logging and Replay

## Summary

### Problem and why we care
Logging is important to analyze attack and repair damage, but current logging systems lacks both completeness and integrity, which could not well support the needs for logging

### Gap in current approaches
Current approach is the direct-on-host approach, where attackers can compromise the log by compromising the kernel, and due to the huge size of log data, logging system cannot produce log that is sufficient to replay the attack

### Hypothesis, Key Idea, or Main Claim
The key idea of ReVirt is to integrate the logging system with the virtual machine, where the whole operating system runs on a single guest process, and the logging system works inside of the VMM layer, thus providing better integrity. Logging only the non deterministic events that will affect the execution flow together with the simplicity of single process logging guarantees the small size of the log thus further guarantees the completeness. During replay the original process execute the deterministic part and the non deterministic data are feed in from the log

### Method for Proving the Claim
Uses micro benchmark to test the correctness of the replay, The first and second micro benchmark proofs that the interrupts are being replayed correctly, the third micro benchmark verified the correctness when replaying external inputs. Uses TCB related proofs to show the integrity of such approach is better than the direct-on-host approach because VMM has much less codes size and much smaller interface to be exploited

### Method for evaluating
Uses benchmark to evaluate the overhead on system and the replay time comparing the the original execution time, and also the log data growth rate, the result is though some system call intense works may suffer from a bigger overhead (1.58 times comparing to direct-on-host), but the overhead on daily usage could be ignorable and is lose to 0. The same conclusion draws from log data storage and replay runtime, which are all good enough for daily usage

### Contributions: what we take away
Virtual machine integrates perfectly with logging, this approach solves both the logging size problem (which further causes completeness problem) and the integrity problem by providing a better TCB

## Pros (3-6 bullets)
Integrity: VMM has much smaller TCB and thus this approach enjoys better integrity
Completeness: by putting the whole OS on a single guest branch and logging only the non deterministic events the log size that is needed to fully replay an attack can be greatly reduced and thus giving full completeness.
Extra tools: logging system also provides extra internal and external tools to support functions to better understand the attacks

## Cons (3-6 bullets)
High overhead for system call heavy apps
May not support apps that uses restricted system calls
Virtual machine approach will introduce portability issues
Multi process tools will greatly increase data interleaving and can cause a lot extra logging, thus may break the completeness

### What is your analysis of the proposed?
I believe this idea of integrating logging system with virtual machine works greatly, where it solve the completeness problem by reducing the required logging data, and VMM also provides a better TCB than the kernel. I believe the idea of this paper have a decent impact on today's logging approach

## Details Comments, Observations, Questions
Can we integrate the logging system into the chip? This approach will also ensures integrity
Can we integrate the logging system into the level of cluster when we trust all the system inside of it? This approach will further greatly reduce the logging size

